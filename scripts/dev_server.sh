#!/bin/sh

rm -Rf bin/shared/
rm -Rf bin/overworld/
rm -Rf bin/tiler/
rm -Rf bin/tiler-processor/

babel \
  --presets=es2015 \
  --ignore=./node_modules/ \
  --out-dir bin/shared/ \
  src/shared/

babel \
  --presets=es2015 \
  --ignore=./node_modules/ \
  --out-dir bin/overworld/ \
  src/overworld/

babel \
  --presets=es2015 \
  --ignore=./node_modules/ \
  --out-dir bin/tiler/ \
  src/tiler/

babel \
  --presets=es2015 \
  --ignore=./node_modules/ \
  --out-dir bin/tiler-processor/ \
  src/tiler-processor/

node ./bin/overworld/index.js &
node ./bin/tiler/index.js &

node ./bin/tiler-processor/index.js &
node ./bin/tiler-processor/index.js &
node ./bin/tiler-processor/index.js &
node ./bin/tiler-processor/index.js
