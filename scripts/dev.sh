#!/bin/sh

export NODE_ENV="development"
export DEBUG="kaku:*"

export KAKU_OVERWORLD_ADDRESS="0.0.0.0"
export KAKU_OVERWORLD_PORT="8080"

export KAKU_DATA_DIR="./tmp/data/"

export KAKU_REDIS_ADDRESS="127.0.0.1"
export KAKU_REDIS_PORT="6379"
unset KAKU_REDIS_PASSWORD

mkdir -p $KAKU_DATA_DIR
nodemon -e js,scss,pug -w ./src/client -w ./src/shared -x ./scripts/dev_client.sh &
nodemon -e js -w ./src/overworld -w ./src/tiler -w ./src/tiler-processor -w ./src/shared -x ./scripts/dev_server.sh
