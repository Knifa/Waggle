#!/bin/sh

export NODE_ENV="production"

rm -Rf bin/client/
rm -Rf bin/shared/
rm -Rf bin/overworld/
rm -Rf bin/tiler/

webpack --config ./src/client/webpack.config.js
pug -o ./bin/client/ ./src/client/pug/index.pug

babel \
  --presets=es2015 \
  --ignore=./node_modules/ \
  --out-dir bin/shared/ \
  src/shared/

babel \
  --presets=es2015 \
  --ignore=./node_modules/ \
  --out-dir bin/overworld/ \
  src/overworld/

  babel \
    --presets=es2015 \
    --ignore=./node_modules/ \
    --out-dir bin/tiler/ \
    src/tiler/
