import getDebugLogger from 'debug'
const debug = getDebugLogger('kaku:overworld:http')

import * as path from 'path'
import express from 'express'
import http from 'http'


const app = express()
const server = http.createServer(app)


app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../../bin/client/index.html'))
})

app.get('/favicon.ico', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../../static/favicon.ico'))
})

app.get('/bundle.js', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../../bin/client/bundle.js'))
})

app.get('/tile', (req, res) => {
  const position = [req.query.x, req.query.y]
  res.sendFile(
    path.resolve(__dirname, '../../', process.env.KAKU_DATA_DIR,
      `${position[0]}_${position[1]}.png`),
    { lastModified: false, headers: { 'Cache-Control': 'no-cache' } }
  )
})

server.listen(
  process.env.KAKU_OVERWORLD_PORT,
  process.env.KAKU_OVERWORLD_ADDRESS)
debug('http listening on %s:%d',
  process.env.KAKU_OVERWORLD_ADDRESS,
  process.env.KAKU_OVERWORLD_PORT)


export default server
