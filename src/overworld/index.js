import 'babel-polyfill'


import getDebugLogger from 'debug'
const debug = getDebugLogger('kaku:overworld:index')


import SocketIoServer from 'socket.io'
import * as redis from 'then-redis'
import * as _ from 'lodash'

import server from './http'
import { Map2D } from '../shared/utils'

import * as sharedConsts from '../shared/consts.js'


const ioServer = SocketIoServer(server)

const redisOptions = {
  host: process.env.KAKU_REDIS_ADDRESS,
  port: process.env.KAKU_REDIS_PORT,
  password: process.env.KAKU_REDIS_PASSWORD
}

const redisClient = redis.createClient(redisOptions)
const redisClientSub = redis.createClient(redisOptions)


let players = []
const tileTimestamps = new Map2D()


class Player {
  constructor(id) {
    this.id = id

    this.name = null
    this.color = '#000000'
    this.position = [0, 0]
    this.painting = false
    this.size = 8
    this.line = []
    this.tileTimestamps = new Map2D()

    this.scale = 1
    this.viewSize = [0, 0]
    this.pan = [0, 0]
  }

  get scaledViewSize() {
    return [
      Math.ceil(this.viewSize[0] / this.scale),
      Math.ceil(this.viewSize[1] / this.scale)
    ]
  }

  inViewRange(position) {
    return _.inRange(
      position[0],
      this.pan[0] - (this.scaledViewSize[0] / 2),
      this.pan[0] + (this.scaledViewSize[0] / 2)
    ) && _.inRange(
      position[1],
      this.pan[1] - (this.scaledViewSize[1] / 2),
      this.pan[1] + (this.scaledViewSize[1] / 2)
    )
  }

  tileInViewRange(tilePosition) {
    const tileViewSize = [
      Math.ceil(this.scaledViewSize[0] / sharedConsts.TILE_SIZE) *
        sharedConsts.TILE_SIZE,
      Math.ceil(this.scaledViewSize[1] / sharedConsts.TILE_SIZE) *
        sharedConsts.TILE_SIZE
    ]

    const xCheck = (offset) => _.inRange(
      tilePosition[0] * sharedConsts.TILE_SIZE + offset,
      this.pan[0] - (tileViewSize[0] / 2),
      this.pan[0] + (tileViewSize[0] / 2))

    const yCheck = (offset) => _.inRange(
      tilePosition[1] * sharedConsts.TILE_SIZE + offset,
      this.pan[1] - (tileViewSize[1] / 2),
      this.pan[1] + (tileViewSize[1] / 2))

    const boundCheck = (xOffset, yOffset) => xCheck(xOffset) && yCheck(yOffset)

    return (
      boundCheck( sharedConsts.TILE_SIZE / 2,  sharedConsts.TILE_SIZE / 2) ||
      boundCheck( sharedConsts.TILE_SIZE / 2, -sharedConsts.TILE_SIZE / 2) ||
      boundCheck(-sharedConsts.TILE_SIZE / 2,  sharedConsts.TILE_SIZE / 2) ||
      boundCheck(-sharedConsts.TILE_SIZE / 2, -sharedConsts.TILE_SIZE / 2)
    )
  }

  sendTiles(positions) {
    for (const position of positions) {
      this.sendTile(position)
    }
  }

  sendTile(position) {
    if (this.tileInViewRange(position)) {
      this.tileTimestamps.set(position, tileTimestamps.get(position))
      ioServer.to(this.id).emit('sync-tile', { position })
    }
  }
}


var syncStaleTiles = _.throttle((player) => {
  const tilesInRange = tileTimestamps.keys().filter((position) => {
    return player.tileInViewRange(position)
  })

  const staleTiles = tilesInRange.filter((position) => {
    return player.tileTimestamps.get(position) === undefined ||
      player.tileTimestamps.get(position) < tileTimestamps.get(position)
  })

  if (staleTiles.length > 0) {
    player.sendTiles(staleTiles)
  }
}, 250)


ioServer.on('connection', (socket) => {
  let player = null;

  socket.on('init', (data, respond) => {
    player = new Player(socket.id)
    player.viewSize = data.viewSize
    player.name = data.name
    players.push(player)

    debug('%s connected with viewsize: %s', player.id, player.viewSize)

    respond({
      id: player.id,
      color: player.color,
      players: players
    })

    redisClient.smembers('tiles')
      .then((redisData) => {
        const positions = redisData.map(JSON.parse)
        const inRange = positions.filter((p) => player.tileInViewRange(p))

        player.sendTiles(inRange)
      })

    ioServer.emit('player-connected', {
      id: player.id,
      color: player.color,
      name: player.name
    })
  })

  socket.on('disconnect', () => {
    ioServer.emit('player-disconnected', { id: socket.id })
    players = players.filter((player) => { return player.id !== socket.id })
  })

  socket.on('sync', (data) => {
    const player = players.find((player) => { return player.id === socket.id })
    if (!player)
      return

    player.position = data.position

    player.color = data.color
    player.painting = data.painting
    player.line = data.line
    player.size = data.size

    player.pan = data.pan
    player.scale = data.scale

    syncStaleTiles(player)
  })

  socket.on('sync-resize', (data) => {
    const player = players.find((player) => { return player.id === socket.id })
    if (!player)
      return

    player.viewSize = data.viewSize
  })

  socket.on('sync-name', (data) => {
    const player = players.find((player) => { return player.id === socket.id })
    if (!player)
      return

    player.name = data.name
  })

  socket.on('paint', (data) => {
    const player = players.find((player) => { return player.id === socket.id })
    player.line = data.line
    player.color = data.color
    player.painting = false
    player.line = []

    redisClient.lpush('tiles.incoming', JSON.stringify({
      points: data.line,
      color: data.color
    }))
  })
})

setInterval(() => {
  ioServer.volatile.emit('sync', { players })
}, sharedConsts.REMOTE_SYNC_TIME)


redisClient.smembers('tiles')
  .then((redisData) => {
    const positions = redisData.map(JSON.parse)
    for (const position of positions) {
      tileTimestamps.set(position, 0)
    }
  })

redisClientSub.subscribe('tiles.done-channel')
redisClientSub.on('message', (channel, message) => {
  const data = JSON.parse(message)
  tileTimestamps.set(
    data.position,
    (tileTimestamps.get(data.position) || 0) + 1)

  for (const p of players)
    p.sendTile(data.position)
})
