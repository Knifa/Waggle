export default new class DeltaTime {
  constructor() {
    this.lastTime = 0

    this.time = 0
    this.delta = 0
  }

  tick() {
    this.time = window.performance.now()
    this.delta = this.time - this.lastTime

    this.lastTime = this.time
  }
}
