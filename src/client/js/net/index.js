import io from 'socket.io-client'

import tileStore from '../stores/tileStore'
import playerStore from '../stores/playerStore'
import viewStore from '../stores/viewStore'
import * as sharedConsts from '../../../shared/consts'


let _socket = null


function connect(initData) {
  return new Promise((resolve) => {
    _socket = io()

    _socket.emit(
      'init',
      {
        viewSize: initData.viewSize,
        name: initData.name
      },
      (response) => {
        playerStore.initLocal({
          id: response.id,
          color: response.color,
          name: initData.name
        })

        for (const remotePlayer of response.players) {
          playerStore.addRemote(remotePlayer)
        }

        _socket.on('player-connected', _onPlayerConnected)
        _socket.on('player-disconnected', _onPlayerDisconnected)
        _socket.on('sync', _onSync)
        _socket.on('sync-tile', _onSyncTile)

        window.setInterval(_sendSync, sharedConsts.LOCAL_SYNC_TIME)
        resolve()
      }
    )
  })
}

function _onPlayerConnected(data) {
  playerStore.addRemote(data)
}

function _onPlayerDisconnected(data) {
  playerStore.removeRemote(data.id)
}

function _onSync(data) {
  for (let player of data.players) {
    playerStore.syncRemote(player)
  }
}

function _onSyncTile(data) {
  window.fetch(`/tile?x=${data.position[0]}&y=${data.position[1]}`)
    .then((res) => res.blob())
    .then((blob) => {
      const blobUrl = window.URL.createObjectURL(blob)
      const img = new Image()
      img.src = blobUrl
      img.addEventListener('load', () => {
        tileStore.tileSet.initTile(data.position, img)
        window.URL.revokeObjectURL(blobUrl)
      })
    })
}

function _sendSync() {
  _socket.emit('sync', {
    position: playerStore.local.position,
    painting: playerStore.local.painting,
    line: playerStore.local.line,
    size: playerStore.local.size,
    color: playerStore.local.color,

    pan: viewStore.pan,
    scale: viewStore.scale
  })
}

function sendPaint(line, color) {
  _socket.emit('paint', {
    line,
    color
  })
}

function sendSyncResize() {
  _socket.emit('sync-resize', {
    viewSize: viewStore.size
  })
}

function sendSyncName() {
  _socket.emit('sync-name', {
    name: playerStore.local.name
  })
}


export default {
  connect,
  sendPaint,
  sendSyncResize,
  sendSyncName
}
