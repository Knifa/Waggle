import * as _ from 'lodash'

import tileStore from '../stores/tileStore'
import playerStore from '../stores/playerStore'
import settingStore from '../stores/settingStore'
import net from '../net'
import * as lines from '../../../shared/lines.js'


export const addPointAtLocation = _.throttle(() => {
  playerStore.local.line.push({
    position: playerStore.local.position,
    size: playerStore.local.size
  })
}, 5)

export function commitPoints() {
  if (!settingStore.debug.disableLocalPaints)
    lines.renderPlayerLineOnTiles(tileStore.tileSet, playerStore.local)

  net.sendPaint(playerStore.local.line, playerStore.local.color)
}
