import * as mouse from './mouse'
import * as touch from './touch'
import * as ui from './ui'


function onToggleControls() {
  window.document
    .getElementById('controls').classList.toggle('is-hidden')

  window.document
    .getElementById('toggle-controls-container').classList.toggle('is-toggled')
}


export function attach(canvasDom) {
  mouse.attach(canvasDom)
  touch.attach(canvasDom)
  ui.attach(canvasDom)

  window.document
    .getElementById('toggle-controls')
    .addEventListener('click', onToggleControls)
}
