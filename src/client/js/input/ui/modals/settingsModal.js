import playerStore from '../../../stores/playerStore'
import settingStore from '../../../stores/settingStore'
import net from '../../../net'


function onHelpClick() {
  window.document.getElementById('settings-modal')
    .classList.toggle('is-visible')
}

function onCloseClick() {
  window.document.getElementById('settings-modal')
    .classList.toggle('is-visible')
}

function onCanvasClick() {
  window.document.getElementById('settings-modal')
    .classList.remove('is-visible')
}


function onNameUnfocus(e) {
  playerStore.local.name = e.currentTarget.value
  window.localStorage.set('name', playerStore.local.name)
  net.sendSyncName()
}

function onShowTileOverlayChange(e) {
  settingStore.debug.showTileOverlay = e.currentTarget.checked
}

function onDisableLocalPaints(e) {
  settingStore.debug.disableLocalPaints = e.currentTarget.checked
}



export function attach() {
  window.document.getElementById('settings-button')
    .addEventListener('click', onHelpClick)

  window.document.getElementById('settings-modal')
    .querySelector('button.modal-close')
    .addEventListener('click', onCloseClick)

  window.document.getElementById('canvas')
    .addEventListener('mousedown', onCanvasClick)

  window.document.getElementById('canvas')
    .addEventListener('touchstart', onCanvasClick)

  window.document.getElementById('settings-name').value =
    window.localStorage.getItem('name')
  window.document.getElementById('settings-name')
    .addEventListener('blur', onNameUnfocus)

  window.document.getElementById('settings-show-tile-overlay')
    .addEventListener('change', onShowTileOverlayChange)

  window.document.getElementById('settings-disable-local-paints')
    .addEventListener('change', onDisableLocalPaints)

  Array.from(window.document.querySelectorAll('.modal-container form'))
    .map((form) => {
      form.addEventListener('submit', (e) => e.preventDefault())
    })
}
