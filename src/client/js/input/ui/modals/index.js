import * as helpModal from './helpModal'
import * as settingsModal from './settingsModal'


export function attach() {
  helpModal.attach()
  settingsModal.attach()
}
