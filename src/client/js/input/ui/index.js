import * as dom from './dom'
import * as colorPicker from './colorPicker'
import * as tools from './tools'
import * as modals from './modals'


export function attach() {
  dom.attach()
  colorPicker.attach()
  tools.attach()
  modals.attach()
}
