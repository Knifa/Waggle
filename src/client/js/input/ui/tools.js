import * as consts from '../../consts'
import viewStore from '../../stores/viewStore'


const toolButtons = {
  [consts.TOOL_PEN]: null,
  [consts.TOOL_PAN]: null,
  [consts.TOOL_ZOOM]: null
}


function onToolClick(e, tool) {
  toolButtons[viewStore.tool].classList.remove('is-selected')
  e.currentTarget.classList.add('is-selected')

  viewStore.tool = tool
}


export function attach() {
  toolButtons[consts.TOOL_PEN] = window.document.getElementById('tool-pen')
  toolButtons[consts.TOOL_PAN] = window.document.getElementById('tool-pan')
  toolButtons[consts.TOOL_ZOOM] = window.document.getElementById('tool-zoom')

  toolButtons[consts.TOOL_PEN]
    .addEventListener('click', (e) => onToolClick(e, consts.TOOL_PEN))
  toolButtons[consts.TOOL_PAN]
    .addEventListener('click', (e) => onToolClick(e, consts.TOOL_PAN))
  toolButtons[consts.TOOL_ZOOM]
    .addEventListener('click', (e) => onToolClick(e, consts.TOOL_ZOOM))
}
