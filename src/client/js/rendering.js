import tinycolor from 'tinycolor2'

import playerStore from './stores/playerStore'
import viewStore from './stores/viewStore'
import settingStore from './stores/settingStore'
import tileStore from './stores/tileStore'
import * as consts from '../../shared/consts.js'
import * as lines from '../../shared/lines.js'
import { drawCircle } from '../../shared/canvas'


export class Renderer {
  constructor(dom) {
    this.dom = dom
    this.ctx = this.dom.getContext('2d')

  }

  render() {
    this.ctx.setTransform(1, 0, 0, 1, 0, 0)
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height)
    this.ctx.setTransform(
      viewStore.scale,
      0,
      0,
      viewStore.scale,
      (this.ctx.canvas.width / 2) - (viewStore.pan[0] * viewStore.scale),
      (this.ctx.canvas.height / 2) - (viewStore.pan[1] * viewStore.scale)
    )

    this._renderTiles(viewStore.pan, viewStore.scale)

    if (playerStore.local.painting) {
      lines.renderPlayerLinePreview(this.ctx, playerStore.local)
    }
    for (let player of playerStore.remotes) {
      if (player.painting) {
        lines.renderPlayerLinePreview(this.ctx, player)
      }
    }

    this._renderPlayerCursor(playerStore.local)
    for (let player of playerStore.remotes) {
      this._renderPlayerCursor(player)
    }
  }

  _renderPlayerCursor(player) {
    const fillStyle = tinycolor(player.color)
    const fillShadowStyle = fillStyle.clone().setAlpha(0.25).darken(25)

    this.ctx.fillStyle = fillShadowStyle.toRgbString()

    this.ctx.beginPath()
    drawCircle(this.ctx, player.position, player.size / 2 * 4 / viewStore.scale)
    this.ctx.fill()
    this.ctx.closePath()

    this.ctx.fillStyle = fillStyle.toHexString()
    this.ctx.strokeStyle = fillShadowStyle.setAlpha(1)
    this.ctx.lineWidth = 1 / viewStore.scale

    this.ctx.beginPath()
    drawCircle(this.ctx, player.position, player.size / 2)
    this.ctx.fill()
    this.ctx.stroke()
    this.ctx.closePath()

    if (player !== playerStore.local) {
      this.ctx.fillStyle = fillStyle.toHexString()
      this.ctx.strokeStyle = fillShadowStyle.setAlpha(1)

      this.ctx.textAlign = 'center'
      this.ctx.textBaseline = 'bottom'
      this.ctx.font = `700 ${Math.round(36 / viewStore.scale)}px "Noto Sans"`

      this.ctx.fillText(
        player.name,
        player.position[0],
        player.position[1] - (player.size / 2 * 4 / viewStore.scale))

      this.ctx.lineWidth = 1 / viewStore.scale
      this.ctx.strokeText(
        player.name,
        player.position[0],
        player.position[1] - (player.size / 2 * 4 / viewStore.scale))
    }
  }


  _renderTiles(panPosition, scale) {
    const tilesInViewRange = tileStore.tileSet.findTilesInViewRange(
      [this.ctx.canvas.width, this.ctx.canvas.height],
      panPosition,
      scale
    )

    for (const tile of tilesInViewRange) {
      this.ctx.drawImage(
        tile.ctx.canvas,
        tile.position[0] * consts.TILE_SIZE - (consts.TILE_SIZE / 2),
        tile.position[1] * consts.TILE_SIZE - (consts.TILE_SIZE / 2))

      if (settingStore.debug.showTileOverlay) {
        this.ctx.strokeStyle = 'rgba(0, 0, 0, 0.25)'
        this.ctx.strokeRect(
          tile.position[0] * consts.TILE_SIZE - (consts.TILE_SIZE / 2),
          tile.position[1] * consts.TILE_SIZE - (consts.TILE_SIZE / 2),
          consts.TILE_SIZE,
          consts.TILE_SIZE
        )

        this.ctx.font = '32px monospace'
        this.ctx.textBaseline = 'top'
        this.ctx.textAlign = 'left'
        this.ctx.fillStyle = 'rgba(0, 0, 0, 0.25)'
        this.ctx.fillText(
          `${tile.position[0]}, ${tile.position[1]}`,
          tile.position[0] * consts.TILE_SIZE - (consts.TILE_SIZE / 2) + 16,
          tile.position[1] * consts.TILE_SIZE - (consts.TILE_SIZE / 2) + 16
        )
      }
    }
  }
}
