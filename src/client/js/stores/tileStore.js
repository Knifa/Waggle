import { TileSet } from '../../../shared/tiles'


function _canvasCtxCreator(width, height) {
  const ctx = document.createElement('canvas').getContext('2d')
  ctx.canvas.width = width
  ctx.canvas.height = height

  return ctx
}


class _TileStore {
  constructor() {
    this._tileSet = new TileSet(_canvasCtxCreator)
  }

  get tileSet() { return this._tileSet }
}


export default new _TileStore()
