class _SettingStore {
  constructor() {
    this.debug = {
      showTileOverlay: false,
      disableLocalPaints: false
    }
  }
}


export default new _SettingStore()
