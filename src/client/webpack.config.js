const path = require('path')
const autoprefixer = require('autoprefixer')
const webpack = require('webpack')


const configDefaults = {
  context: path.resolve(__dirname),
  entry: ['babel-polyfill', path.resolve(__dirname, './js/index.js')],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '../../bin/client/')
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: 'es2015'
        }
      },
      {
        test: /\.scss$/,
        loader: 'style!css!postcss!sass',
      }
    ]
  },

  postcss: [autoprefixer],
};

const configDev = {
  devtool: 'inline-source-map'
}

const configProduction = {
  plugins: [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false,
      compress: {
        warnings: false
      }
    })
  ]
}


if (process.env.NODE_ENV === 'production')
  module.exports = Object.assign({}, configDefaults, configProduction)
else
  module.exports = Object.assign({}, configDefaults, configDev)
