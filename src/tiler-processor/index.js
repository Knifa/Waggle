import 'babel-polyfill'

import getDebugLogger from 'debug'
const debug = getDebugLogger('kaku:tiler-processor:index')


import * as redis from 'then-redis'
import tiles from './tiles'


const redisClient = redis.createClient({
  host: process.env.KAKU_REDIS_ADDRESS,
  port: process.env.KAKU_REDIS_PORT,
  password: process.env.KAKU_REDIS_PASSWORD
})


function wait() {
  redisClient.brpoplpush('tiles.ready', 'tiles.processing', 0)
    .then((redisData) => {
      const data = JSON.parse(redisData)
      debug(`got paint for tile ${data.position}`)

      tiles.paint(data.position, data.paint.points, data.paint.color)

      redisClient.multi()
      redisClient.sadd('tiles', JSON.stringify(data.position))
      redisClient.lrem('tiles.processing', -1, redisData)
      redisClient.publish('tiles.done-channel', redisData)
      redisClient.exec()

      wait()
    })
    .catch(debug)
}


debug('ready!')
wait()
