import getDebugLogger from 'debug'
const debug = getDebugLogger('kaku:tiler-processor:tiles')


import Canvas from 'canvas'

import { Tile } from '../shared/tiles'
import * as lines from '../shared/lines'
import * as tileIo from './io'


function paint(tilePos, points, color) {
  const tile = new Tile(
    (width, height) => new Canvas(width, height).getContext('2d'),
    tilePos)

  tileIo.loadTile(tilePos, tile)
  tile.renderOn((ctx) => lines.renderLine(ctx, points, color))
  tileIo.writeOut(tile)
}


export default {
  paint
}
