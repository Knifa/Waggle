import getDebugLogger from 'debug'
const debug = getDebugLogger('kaku:tiler:io')


import fs from 'fs'
import path from 'path'
import { Image } from 'canvas'


const dataPath = process.env.KAKU_DATA_DIR


export function loadTile(position, tile) {
  try {
    const imgData = fs.readFileSync(path.join(dataPath,
      `${position[0]}_${position[1]}.png`))
    const img = new Image()
    img.src = imgData

    tile.init(img)
  } catch (err) {
    // Ignore missing files (TileSet will fill in the blanks)
  }
}

export function writeOut(tile) {
  const imagePath = path.join(
    dataPath,
    `${tile.position[0]}_${tile.position[1]}.png`)
  const imagePathTmp = `${imagePath}_`
  const imageBuffer = tile.ctx.canvas.toBuffer()

  fs.writeFileSync(imagePathTmp, imageBuffer)
  fs.renameSync(imagePathTmp, imagePath)
}
